*** Settings ***
Documentation     A test suite with a single test for logging in as existing user.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file. It makes sure that an existing user
...               is able to log into the system.

Resource          finnkino-resource.robot
Test Teardown     Close Browser

*** Test Cases ***
Succesful Login
    Logging In To Finnkino Should Be Possible

