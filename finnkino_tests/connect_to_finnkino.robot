*** Settings ***
Documentation     A test suite with a single test for good connection.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file. It makes sure that Finnkino's
...               domain is up and running.

Resource          finnkino-resource.robot
Test Teardown     Close Browser

*** Test Cases ***
Successful Connection
    Given Browser Is Opened To Finnkino Main Page
    Then Popup Should Be Closed
    And Main Page Should Be Open